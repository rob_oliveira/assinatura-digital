require 'openssl'


require 'origami'

include Origami

pdf = PDF.new

pdf.append_page
pdf.pages.first.write "Hello", size: 20

pdf.save("example.pdf")

# Another way of doing it
PDF.write("example.pdf") do |pdf|
    pdf.append_page do |page|
        page.write "Hello", size: 30
    end
    
        puts "This document has #{pdf.pages.size} page(s)"
        page = pdf.get_page(1)
        
          width = 200.0
          height = 50.0

          x = 10

          y = 20
          size = 8
        
          now = Time.now
        
          text_annotation = Annotation::AppearanceStream.new
          text_annotation.Type = Origami::Name.new("XObject")
          text_annotation.Resources = Resources.new
          text_annotation.Resources.ProcSet = [Origami::Name.new("Text")]
          text_annotation.set_indirect(true)
          text_annotation.Matrix = [ 1, 0, 0, 1, 0, 0 ]
          text_annotation.BBox = [ 0, 0, width, height ]
        text_annotation.write("Signed at", x: size, y: (height/2)-(size/2), size: size)


        pdf.save('example2.pdf')
    
end
