#!/usr/bin/env ruby

require "openssl"
require "time"

begin
  require "origami"
rescue LoadError
  abort "origami not installed: gem install origami"
end
include Origami

CERT_FILE = "server.crt"
KEY_FILE = "nopassword.key"


CERT_FILE_2 = "certificate.crt"
KEY_FILE_2 = "privateKey.key"


input_files = ARGV

if input_files.empty?
  abort "Usage: sign-pdf input.pdf [...]"
end

key = OpenSSL::PKey::RSA.new(File.read(KEY_FILE))
cert = OpenSSL::X509::Certificate.new(File.read(CERT_FILE))

key_2 = OpenSSL::PKey::RSA.new(File.read(KEY_FILE))
cert_2 = OpenSSL::X509::Certificate.new(File.read(CERT_FILE))

input_files.each do |file|
  output_filename = file.dup.insert(file.rindex("."), "_signed")


    
  pdf = PDF.read(file)
  page = pdf.get_page(2)

  width = 200.0
  height = 50.0
  x = page.MediaBox[2].to_f - width - height
  y = height
  size = 8


  now = Time.now

  text_annotation = Annotation::AppearanceStream.new
  text_annotation.Type = Origami::Name.new("XObject")
  text_annotation.Resources = Resources.new
  text_annotation.Resources.ProcSet = [Origami::Name.new("Text")]
  text_annotation.set_indirect(true)
  text_annotation.Matrix = [ 1, 0, 0, 1, 0, 0 ]
  text_annotation.BBox = [ 0, 0, width, height ]
  text_annotation.write("Assinado em #{now.iso8601}", x: size, y: (height/2)-(size/2), size: size)

  text_annotation_2 = Annotation::AppearanceStream.new
  text_annotation_2.Type = Origami::Name.new("XObject")
  text_annotation_2.Resources = Resources.new
  text_annotation_2.Resources.ProcSet = [Origami::Name.new("Text")]
  text_annotation_2.set_indirect(true)
  text_annotation_2.Matrix = [ 1, 0, 0, 1, 0, 0 ]
  text_annotation_2.BBox = [ 0, 0, width, height ]
  text_annotation_2.write("Assinado em #{now.iso8601}", x: size, y: (height/2)-(size/2), size: size)

  # Add signature annotation (so it becomes visibles in PDF document)
  signature_annotation = Annotation::Widget::Signature.new
  signature_annotation.Rect = Rectangle[llx: x, lly: y+height, urx: x+width, ury: y]
  signature_annotation.F = Annotation::Flags::PRINT
  signature_annotation.set_normal_appearance(text_annotation)


  signature_annotation_2 = Annotation::Widget::Signature.new
  signature_annotation_2.Rect = Rectangle[llx: 0, lly: y+height, urx: width, ury: y]
  signature_annotation_2.F = Annotation::Flags::PRINT
  signature_annotation_2.set_normal_appearance(text_annotation_2)


  page.add_annotation(signature_annotation)
  page.add_annotation(signature_annotation_2)

  # Sign the PDF with the specified keys
  pdf.sign(cert, key,
    method: Signature::PKCS7_DETACHED,
    annotation: signature_annotation,
    location: "Helsinki",
    contact: "contact@kiskolabs.com",
    reason: "Proof of Concept"

  )


  pdf.sign(cert_2, key_2,
    method: Signature::PKCS7_DETACHED,
    annotation: signature_annotation_2,
    location: "Fortaleza",
    contact: "teste@teste.com",
    reason: "Só pra ver se ta maroto"

  )






  puts pdf.signed?
  # Save the resulting file
  pdf.save(output_filename)
end
